# Plumber

## 개요

InfoGrab 프로덕트 팀에서 사용하는 커밋 컨벤션입니다. 또한 Husky와 Commitlint를 사용하여 커밋 메시지를 자동화합니다.

## 커밋 컨벤션 정의

보다 상세한 내용은 [커밋 컨벤션 정의](./commitlint.config.ts)를 참고하세요.

아래는 Commit Message의 형식을 정의한 것입니다.

``` plain
<emoji><type>(<scope>): <subject>

<body>

<footer>
```

- Type(필수): Commit의 종류. commit을 할 때, type에 상응하는 이모지가 자동으로 붙습니다.  
  (Feat, Fix, Style, Refactor, File, Design, Comment, Chore, Docs, Hotfix)
- Scope(선택): Commit의 범위. 기능, 함수, 페이지, API 등 자유롭게 선택할 수 있습니다.
- Subject(필수): Commit의 제목. 되도록 간결하게 작성하고, 명사형 어미로 끝나도록 합니다.
- Body(선택): Commit의 내용. 어떤 이유로, 어떻게 변경했는지 작성합니다.
- Footer(자동): Issue Tracker ID가 자동으로 삽입됩니다. Branch 이름이 issue<id>- 형식이어야 합니다.

## Commit 메시지 자동화

### 준비하기

1. 프로젝트 폴더에서 `npm install` 후 `npm run prepare`를 실행합니다.
2. `husky - Git hooks installed` 메시지가 보이면 설정을 완료한 것입니다.

### 커밋하는 법

아래 예시처럼 `<type>(optional scope): <subject>` 형식으로 커밋 메시지를 작성합니다.

``` plain
feat: second commit
```

그러면 실제 커밋은 아래처럼 이루어집니다.

``` plain
✨ Feat: second commit

#393
```

### 기능 설명

- 이슈 번호는 브랜치에서 자동으로 가져와 Footer에 추가됩니다.
- 이미 이슈번호가 포함된 Footer가 있을 경우 이슈번호가 추가되지 않습니다.
- 이모지는 `.husky/prepare-commit-msg`에 매핑되어 있습니다.
- 만약 커밋 메시지가 `Merge|Revert|Amend|Reset|Rebase|Tag` 중 하나일 경우 [자동화를 무시](./commitlint.config.ts#L68)합니다.
- Type(feat, fix, ...)의 첫 글자는 대문자로 자동 변환됩니다.
